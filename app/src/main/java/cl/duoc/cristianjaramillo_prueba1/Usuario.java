package cl.duoc.cristianjaramillo_prueba1;

/**
 * Created by DUOC on 22-04-2017.
 */

public class Usuario {

    private String usuario,contra,repetriContra;

    public Usuario() {
    }

    public Usuario(String usuario, String contra, String repetriContra) {
        setContra(contra);
        setRepetriContra(repetriContra);
        setUsuario(usuario);
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContra() {
        return contra;
    }

    public void setContra(String contra) {
        this.contra = contra;
    }

    public String getRepetriContra() {
        return repetriContra;
    }

    public void setRepetriContra(String repetriContra) {
        this.repetriContra = repetriContra;
    }
}
