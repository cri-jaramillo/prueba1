package cl.duoc.cristianjaramillo_prueba1;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    Button btnEntrar,btnRegistrar;
    EditText edtUsuario,edtContra;
    BaseDeDatos lista = new BaseDeDatos();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtUsuario = (EditText)findViewById(R.id.edtUsuario);
        edtContra = (EditText)findViewById(R.id.edtPassword);
        btnRegistrar =(Button)findViewById(R.id.btnRegistrar);
        btnEntrar = (Button)findViewById(R.id.btnEntrar);

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();


            }
        });

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LoginActivity.this, RegistroActivity.class);
                startActivity(intent);
            }
        });

    }

    public void login()
    {
        String contra=edtContra.getText().toString();
        String usuario = edtUsuario.getText().toString();
        if(lista.login(contra,usuario)== true)
        {
            Toast.makeText(this, "Logueo con exito", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(LoginActivity.this, ListadoUsuariosActivity.class);
            startActivity(intent);

        }else
        {
            Toast.makeText(this, "contraseña o usuario incorrento", Toast.LENGTH_SHORT).show();
        }

    }




}
