package cl.duoc.cristianjaramillo_prueba1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by DUOC on 22-04-2017.
 */

public class RegistroActivity extends AppCompatActivity {

    EditText edtUsuario,edtContra,edtRepetirContra;
    Button btnRegistro, btnVolver;

    BaseDeDatos lista = new BaseDeDatos();
    Usuario us = new Usuario();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);


        edtUsuario = (EditText)findViewById(R.id.edtregUsuario);
        edtContra = (EditText)findViewById(R.id.edtRegContraseña);
        edtRepetirContra = (EditText)findViewById(R.id.edtRegRepetir);
        btnRegistro = (Button)findViewById(R.id.bntRegistro);
        btnVolver = (Button)findViewById(R.id.btnVolver);


        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(edtContra.getText().toString().equals(edtRepetirContra.getText().toString()))
                {
                    AgregarUsuario();
                    Toast.makeText(RegistroActivity.this, "usuario agregado con exito", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(RegistroActivity.this, "claves no coinciden", Toast.LENGTH_SHORT).show();
                }

            }
        });
        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegistroActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }


    public void AgregarUsuario()
    {
            us = new Usuario(edtUsuario.getText().toString(),edtContra.getText().toString(),edtRepetirContra.toString());
            lista.agregarUsuario(us);


    }



}
