package cl.duoc.cristianjaramillo_prueba1;


import java.util.ArrayList;

/**
 * Created by DUOC on 22-04-2017.
 */

public class BaseDeDatos {
    private static ArrayList<Usuario> values = new ArrayList<>();

    public static void agregarUsuario(Usuario usuario){
            values.add(usuario);

    }

    public static boolean login (String usuario, String contra)
    {

        boolean resp = false;
        for (Usuario x : values )
            if (x.getUsuario().equals(usuario) && x.getContra().equals(contra)) {

                resp = true;
            } else {
                resp = false;
            }
        return resp;
    }

    public static ArrayList<Usuario> obtieneListadoUsuarios(){
        return values;
    }
}

